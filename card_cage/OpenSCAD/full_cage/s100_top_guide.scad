// This is the top side with card guides.
// Orientation:
// x = 0 : left edge of backplane PCB
// y = 0 : top of backplane
// z = 0 : outermost surface of card guide

include <s100_defs.scad>

$fn = 40;

lip_to_mod = (bp_hole_row3 - bp_hole_row2)/2 + bp_hole_row2 + mod_width/2 - mod_ofs - bp_height - edge[3];
lip_z = lip_to_mod + cg_slot_gap + cg_depth;
screw_cb = lip_z - attach_thick;

difference(){
  // Additions (guides, frame, lip, etc)
  union(){
    base_lip(lip_z, 0);
    card_guides(0);
    add_frame(lip_z, 0);
  }
  
  // Removals (holes + slot cutouts)
  mtg_holes(0);
  card_slots(0);
  attach_holes(0); 
  attach_cutout(0);
  support_holes(screw_threads, 1);
  top_chamfer(0);
}

