;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;   Program that tests RAM and outputs test stripes                                        ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ORG 0x0000
;------------------------------------------------------------------------------

MAIN:           LD HL, 0x01FF        ; Set the ram address
                LD SP, HL            ; Set up a temporary stack
                LD C,GRAM            ; Mem for graphics
                LD B,COUNT           ; Counter
                LD A,COLOUR          ; Colour to A
                LD (ADDR),A          ; Out to data bus to ram

FOREVER:

          LD A,(ADDR)          ; colour in from ram
          OUT (C),A            ; Out to graphics ram via I/O
          INC A                ; Next colour
          LD (ADDR),A          ; colour out to ram
          CP PALETTE           ; 8 colours
          JP NZ,NXT            ; Reached 6th colour reset colour
          LD A,COLOUR          ; First colour back in to A
          LD (ADDR), A         ; reset colour in ram
NXT:

          INC C                ; Increment ram addr
          INC B                ; Using B for buffer size ( ugly )
          LD A,B               ; B in to A
          CP BUFFER              ; Have we filled buffer
          JP NZ,FOREVER        ; Keep going till buffer filled

HALT

ADDR: equ 0x100   ; ram address
GRAM: equ 0x00    ; graphics ram
COLOUR: equ 0x00  ; default colour
COUNT: equ 0x00   ; counter starts from
PALETTE: equ 0x08 ; size of palette
BUFFER: equ 0x40  ; size of graphics buffer

;------------------------------------------------------------------------------
