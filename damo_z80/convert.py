import binascii
file = open("a.bin", "rb")
byte = file.read(1)
str = "v2.0 raw\n"
while byte:
    str += binascii.hexlify(byte)+" "
    byte = file.read(1)
file.close()
print(str)
